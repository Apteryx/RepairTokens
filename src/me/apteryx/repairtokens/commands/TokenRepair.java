package me.apteryx.repairtokens.commands;

import me.apteryx.repairtokens.RepairTokens;
import me.apteryx.repairtokens.utils.MessageUtils;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

/**
 * @author apteryx
 * @time 12:48 PM
 * @since 8/13/2016
 */
public class TokenRepair implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        switch(command.getName().toLowerCase()) {
            case"tokenrepair":
                if (commandSender.hasPermission("repairtokens.repair")) {
                    if (commandSender instanceof Player) {
                        Player player = (Player) commandSender;
                        if (player.getItemInHand() == null || player.getItemInHand().getType() != Material.STICK) {
                            commandSender.sendMessage(MessageUtils.translateMessage("tokenrepair_failed_item"));
                            return true;
                        }
                        if (RepairTokens.plugin.getTokens().get(player.getUniqueId()) > 0) {
                            RepairTokens.plugin.getTokens().replace(player.getUniqueId(), RepairTokens.plugin.getTokens().get(player.getUniqueId()), RepairTokens.plugin.getTokens().get(player.getUniqueId()) - 1);
                            handleRepair(player.getItemInHand(), player);
                            player.playSound(player.getLocation(), Sound.ITEM_PICKUP, 1F, 1F);
                            player.sendMessage(MessageUtils.translateMessage("tokenrepair_success_item"));
                            RepairTokens.plugin.saveTokens();
                            return true;
                        } else {
                            commandSender.sendMessage(MessageUtils.translateMessage("tokenrepair_failed_balance"));
                            return true;
                        }
                    }
                }
        }
        return false;
    }

    private void handleRepair(ItemStack item, Player player) {
        if (item == null) {
            return;
        }
        if (item.getItemMeta().hasLore()) {
            String pickMaterial = item.getItemMeta().getLore().get(item.getItemMeta().getLore().size() - 1);
            pickMaterial = pickMaterial.replaceAll("\2470", "");
            ItemStack itemPick = new ItemStack(Material.getMaterial(pickMaterial));
            List<String> newLore = item.getItemMeta().getLore();
            newLore.remove("\2470" + pickMaterial);
            ItemMeta pickMeta = itemPick.getItemMeta();
            pickMeta.setLore(newLore);
            pickMeta.setDisplayName(item.getItemMeta().getDisplayName());
            itemPick.setItemMeta(pickMeta);
            itemPick.addEnchantments(item.getEnchantments());
            player.getInventory().removeItem(item);
            player.getInventory().addItem(itemPick);
        }
    }
}
