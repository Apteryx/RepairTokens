package me.apteryx.repairtokens.commands;

import me.apteryx.repairtokens.RepairTokens;
import me.apteryx.repairtokens.utils.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author apteryx
 * @time 12:13 PM
 * @since 8/13/2016
 */
public class TokenBalance implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        switch (command.getName().toLowerCase()) {
            case"tokenbalance":
                if (commandSender.hasPermission("repairtokens.balance")) {
                    if (commandSender instanceof Player) {
                        if (strings.length == 0) {
                            Player player = (Player) commandSender;
                            String messageSuccess = MessageUtils.translateMessage("tokenbalance_self_success").replace("{tokens}", RepairTokens.plugin.getTokens().get(player.getUniqueId()).toString());
                            commandSender.sendMessage(messageSuccess);
                            return true;
                        }

                    }else{
                        commandSender.sendMessage("Only players are allowed to execute this particular command.");
                        return true;
                    }
                }

               if (commandSender.hasPermission("repairtokens.balance.others")) {
                   Player player = Bukkit.getPlayer(strings[0]);
                   if (player == null) {
                       commandSender.sendMessage(MessageUtils.translateMessage("tokenbalance_others_invalid"));
                       return true;
                   }
                   String message = MessageUtils.translateMessage("tokenbalance_others_success").replace("{tokens}", RepairTokens.plugin.getTokens().get(player.getUniqueId()).toString()).replace("{player}", player.getName());
                   commandSender.sendMessage(message);
                   return true;
               }
        }
        return false;
    }
}
