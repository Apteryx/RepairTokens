package me.apteryx.repairtokens.commands;

import me.apteryx.repairtokens.RepairTokens;
import me.apteryx.repairtokens.utils.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author apteryx
 * @time 11:49 AM
 * @since 8/13/2016
 */
public class AddTokens implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        switch(command.getName().toLowerCase()) {
            case"addtokens":
                if (commandSender.hasPermission("repairtokens.addtokens")) {
                    if (args.length == 0 || args.length == 1) {
                        return false;
                    }

                    Player player = Bukkit.getPlayer(args[0]);
                    if (player == null) {
                        commandSender.sendMessage(MessageUtils.translateMessage("addtokens_failed"));
                        return false;
                    }

                    if (RepairTokens.plugin.getTokens().containsKey(player.getUniqueId())) {
                        RepairTokens.plugin.getTokens().replace(player.getUniqueId(), RepairTokens.plugin.getTokens().get(player.getUniqueId()), RepairTokens.plugin.getTokens().get(player.getUniqueId()) + Integer.parseInt(args[1]));
                        commandSender.sendMessage(MessageUtils.translateMessage("addtokens_success").replace("{tokens}", args[1]).replace("{player}", player.getName()));
                        RepairTokens.plugin.saveTokens();
                        return true;
                    } else {
                        commandSender.sendMessage("The player seems to have no data, contact Apteryx something messed up.");
                        return true;
                    }
                }
        }
        return false;
    }
}
