package me.apteryx.repairtokens.commands;

import me.apteryx.repairtokens.RepairTokens;
import me.apteryx.repairtokens.utils.MessageUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * @author apteryx
 * @time 12:09 PM
 * @since 8/13/2016
 */
public class SaveTokens implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        switch(command.getName().toLowerCase()) {
            case"savetokens":
                if (commandSender.hasPermission("repairtokens.savetokens")) {
                    RepairTokens.plugin.saveTokens();
                    commandSender.sendMessage(MessageUtils.translateMessage("save_success"));
                    return true;
                }
        }
        return false;
    }
}
