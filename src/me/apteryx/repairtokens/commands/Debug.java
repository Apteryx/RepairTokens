package me.apteryx.repairtokens.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * @author apteryx
 * @time 1:46 PM
 * @since 8/13/2016
 */
public class Debug implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        switch(command.getName().toLowerCase()) {
            case"debug":
                final List<String> lore = new ArrayList<>();
                Player player = (Player) commandSender;
                ItemMeta itemMeta = player.getItemInHand().getItemMeta();
                itemMeta.setDisplayName("Meme");
                lore.add("Test lore");
                itemMeta.setLore(lore);
                player.getItemInHand().setItemMeta(itemMeta);
                lore.clear();
                return true;
        }
        return false;
    }
}
