package me.apteryx.repairtokens.tasks;

import me.apteryx.repairtokens.RepairTokens;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author apteryx
 * @time 10:11 AM
 * @since 8/13/2016
 */
public class SaveTask extends BukkitRunnable {

    @Override
    public void run() {
        Bukkit.getLogger().info("Attempting to save token data...");
        RepairTokens.plugin.saveTokens();
    }
}
