package me.apteryx.repairtokens;

import me.apteryx.repairtokens.commands.*;
import me.apteryx.repairtokens.listeners.BlockDamage;
import me.apteryx.repairtokens.listeners.ItemBreak;
import me.apteryx.repairtokens.listeners.PlayerJoin;
import me.apteryx.repairtokens.tasks.SaveTask;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

/**
 * @author apteryx
 * @time 9:10 AM
 * @since 8/13/2016
 */
public class RepairTokens extends JavaPlugin {

    private final HashMap<UUID, Integer> tokens = new HashMap<>();
    private final List<String> items = new ArrayList<>();
    private File tokenFile;
    public static RepairTokens plugin;
    final YamlConfiguration tokenConfiguration = new YamlConfiguration();


    @Override
    public void onEnable() {
        if (plugin == null) {
            plugin = this;
        }

        if (Bukkit.getPluginManager().getPlugin("AutoSell") != null) {
            getLogger().info("AutoSell detected on the server!");
            //BlockBreakEvent.getHandlerList().unregister(Bukkit.getPluginManager().getPlugin("AutoSell"));
            //getLogger().info("Unregistered AutoSell BlockBreakEvent.");
            Bukkit.getPluginManager().registerEvents(new BlockDamage(), this);
            getLogger().info("Fixed conflict with AutoSell.");
        } else {
            Bukkit.getPluginManager().registerEvents(new ItemBreak(), this);
        }
        createTokenFile();
        saveDefaultConfig();
        loadItems();
        Bukkit.getPluginManager().registerEvents(new PlayerJoin(), this);
        new SaveTask().runTaskLaterAsynchronously(this, 150);
        getCommand("addtokens").setExecutor(new AddTokens());
        getCommand("settokens").setExecutor(new TokenSet());
        getCommand("savetokens").setExecutor(new SaveTokens());
        getCommand("tokenbalance").setExecutor(new TokenBalance());
        getCommand("tokenrepair").setExecutor(new TokenRepair());
        //getCommand("debug").setExecutor(new Debug());
        getLogger().info("RepairTokens has been enabled.");
    }

    @Override
    public void onDisable() {
        saveTokens();
        getLogger().info("RepairToken has been disabled.");
    }

    @Override
    public void onLoad() {
        getLogger().info("RepairTokens has loaded.");
    }

    private void createTokenFile() {
        tokenFile = new File(getDataFolder(), "player-tokens.yml");
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
            getLogger().info("mkdir /RepairTokens/");
        }

        if (!tokenFile.exists()) {
            try {
                Files.createFile(tokenFile.toPath());
                getLogger().info("touch /RepairTokens/player-tokens.yml");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadTokens(Player player) {
        try {
            tokenConfiguration.load(tokenFile);
        }catch (Exception e) {
            e.printStackTrace();
        }
        if (tokenConfiguration.contains(player.getUniqueId().toString()) && !tokens.containsKey(player.getUniqueId())) {
            getLogger().info(String.format("Loaded configuration for %s they have a balance of %s.", player.getName(), tokenConfiguration.getInt(player.getUniqueId().toString())));
            tokens.put(player.getUniqueId(), tokenConfiguration.getInt(player.getUniqueId().toString()));
        }else{
            getLogger().warning(String.format("Didn't find any previous data for %s, giving them defaults.", player.getName()));
            tokens.put(player.getUniqueId(), 5);
        }

    }

    public void loadItems() {
        items.clear();
        items.addAll(this.getConfig().getStringList("tools"));
    }

    public void saveTokens() {
        for(Map.Entry<UUID, Integer> tokenConfig : tokens.entrySet()) {
            tokenConfiguration.set(tokenConfig.getKey().toString(), tokenConfig.getValue());
            try {
                tokenConfiguration.save(tokenFile);
                getLogger().info("Saved token data.");
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public HashMap<UUID, Integer> getTokens() {
        return tokens;
    }

    public List<String> getItems() {
        return items;
    }


}
