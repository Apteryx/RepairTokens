package me.apteryx.repairtokens.listeners;

import me.apteryx.repairtokens.RepairTokens;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * @author apteryx
 * @time 9:51 AM
 * @since 8/13/2016
 */
public class PlayerJoin implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Bukkit.getLogger().info("Attempting to load token data for " + event.getPlayer().getName());
        RepairTokens.plugin.loadTokens(event.getPlayer());
    }
}
