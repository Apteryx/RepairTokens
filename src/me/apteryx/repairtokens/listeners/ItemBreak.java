package me.apteryx.repairtokens.listeners;

import me.apteryx.repairtokens.RepairTokens;
import me.apteryx.repairtokens.utils.MessageUtils;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * @author apteryx
 * @time 12:49 PM
 * @since 8/13/2016
 */
public class ItemBreak implements Listener {


    @EventHandler
    public void onBreak(PlayerItemBreakEvent event) {
        //event.getPlayer().sendMessage("Look an item broke.");
        if (RepairTokens.plugin.getItems().contains(event.getBrokenItem().getType().toString())) {
            handleBreaking(event.getBrokenItem(), event.getPlayer());
            //event.getPlayer().sendMessage(MessageUtils.translateMessage("item_broken_event"));
        }


    }

    public void handleBreaking(ItemStack itemStack, Player player) {
        if (itemStack == null) {
            return;
        }

        player.playSound(player.getLocation(), Sound.ITEM_BREAK, 1F, 1F);
        player.sendMessage(MessageUtils.translateMessage("item_broken_event"));
        final List<String> lore = new ArrayList<>();
        player.getInventory().remove(itemStack);
        ItemStack toolHandle = new ItemStack(Material.STICK);
        toolHandle.setItemMeta(itemStack.getItemMeta());
        ItemMeta itemMeta = toolHandle.getItemMeta();
        if (!itemStack.getItemMeta().hasLore()) {
            lore.add("\2470" + itemStack.getType());
            itemMeta.setLore(lore);
        } else if (!itemMeta.getLore().contains("PICKAXE")){
            lore.addAll(itemMeta.getLore());
            lore.add("\2470" + itemStack.getType());
            itemMeta.setLore(lore);
        }
        toolHandle.setItemMeta(itemMeta);
        player.getInventory().addItem(toolHandle);
        lore.clear();
        //toolHandle.getItemMeta().getLore().add("\2470" + itemStack.getType());
    }
}
