package me.apteryx.repairtokens.listeners;

import me.apteryx.repairtokens.RepairTokens;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDamageEvent;

/**
 * @author apteryx
 * @time 5:23 PM
 * @since 8/24/2016
 */
public class BlockDamage implements Listener {

    @EventHandler
    public void onDamageBlock(BlockDamageEvent event) {
        ItemBreak itemBreak = new ItemBreak();
        if (event.getItemInHand().getDurability() == event.getItemInHand().getType().getMaxDurability() - 1 && RepairTokens.plugin.getItems().contains(event.getItemInHand().getType().toString())) {
            itemBreak.handleBreaking(event.getItemInHand(), event.getPlayer());
        }
    }
}
