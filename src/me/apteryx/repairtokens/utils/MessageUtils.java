package me.apteryx.repairtokens.utils;

import me.apteryx.repairtokens.RepairTokens;

/**
 * @author apteryx
 * @time 9:05 PM
 * @since 8/23/2016
 */
public class MessageUtils {

    public static String translateMessage(String message) {
        message = RepairTokens.plugin.getConfig().getString(message);
        message = message.replaceAll("&", "\247");
        return message;
    }
}
